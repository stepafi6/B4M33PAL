#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <bits/stdc++.h>

struct Cluster;

struct Node {
    int index;
    std::list<Node *> neighbors;
    std::list<Node *> invertedNeighbors;
    bool visited = false;
    Node *predecesor = nullptr;
    Cluster *cluster = nullptr;
};

struct Cluster {
    std::vector<Node *> nodes;
    std::list<Cluster *> neighbors;
    std::list<Cluster *> invertedNeighbors;
    std::list<Cluster *> dualNeighbors;
    int distance = INT_MAX;
};

typedef std::vector<Node> uGraph;

void Kosarajus();

void FillOrder(Node *v, std::stack<Node *> &Stack);

void DFS(Node *v, Cluster *pCluster);

void Dijkstra(Cluster *src);


uGraph graph;
std::list<Cluster> clusters;
int factoryCount, conveyorsCount, centralStore;

int main() {
    std::scanf("%d %d %d", &factoryCount, &conveyorsCount, &centralStore);

    graph = std::vector<Node>(factoryCount);
    for (int factoryIndex = 0; factoryIndex < factoryCount; ++factoryIndex) {
        graph[factoryIndex].index = factoryIndex;
    }

    int source, dest;
    for (int i = 0; i < conveyorsCount; ++i) {
        std::scanf("%d %d", &source, &dest);

        graph[source].neighbors.push_back(&graph[dest]);

        graph[dest].invertedNeighbors.push_back(&graph[source]);
    }

    Kosarajus();

    for (auto clusterIt = clusters.begin(); clusterIt != clusters.end(); ++clusterIt) {
        Cluster *cluster = &(*clusterIt);
        for (auto it2 = cluster->nodes.begin(); it2 != cluster->nodes.end(); it2++) {
            Node *node = (*it2);
            for (auto it3 = node->neighbors.begin(); it3 != node->neighbors.end(); it3++) {
                Node *neighbor = (*it3);
                if (neighbor->cluster != node->cluster) {
                    cluster->neighbors.push_back(neighbor->cluster);
                    neighbor->cluster->invertedNeighbors.push_back(cluster);

                    cluster->dualNeighbors.push_back(neighbor->cluster);
                    neighbor->cluster->dualNeighbors.push_back(cluster);
                }
            }
        }
    }

    Dijkstra(graph[centralStore].cluster);

    clusters.sort([](const Cluster &a, const Cluster &b) -> bool {
        if (a.distance > b.distance) return true;
        if (b.distance > a.distance) return false;

        return false;
    });

    int re = 0;
    for (auto it = clusters.begin(); it != clusters.end(); it++) {
        Cluster *cluster = &(*it);

        if (cluster->invertedNeighbors.empty())
            re += cluster->distance;
    }

    std::cout << re << std::endl;

    return 0;
}

void Kosarajus() {
    for (auto &node: graph)
        node.visited = false;

    std::stack<Node *> stack;

    for (auto &node: graph)
        if (!node.visited)
            FillOrder(&node, stack);

    for (auto &node: graph)
        node.visited = false;

    clusters = std::list<Cluster>();

    while (stack.empty() == false) {
        Node *v = stack.top();
        stack.pop();

        if (!v->visited) {
            clusters.push_back({});
            DFS(v, &clusters.back());
            //std::cout << "FOR: " << clusters.back().distance << std::endl;
        }

    }
}

void FillOrder(Node *v, std::stack<Node *> &Stack) {
    v->visited = true;

    std::list<Node *>::iterator i;

    for (i = v->neighbors.begin(); i != v->neighbors.end(); ++i) {
        if (!(*i)->visited) {
            FillOrder(*i, Stack);
        }
    }

    Stack.push(v);
}

void DFS(Node *v, Cluster *pCluster) {
    v->visited = true;

    pCluster->nodes.push_back(v);
    v->cluster = pCluster;

    std::list<Node *>::iterator it;

    for (it = v->invertedNeighbors.begin(); it != v->invertedNeighbors.end(); ++it) {
        if (!(*it)->visited)
            DFS((*it), pCluster);
    }
}

class MyCompare {
public:
    bool operator()(Cluster *a, Cluster *b) {
        return (*a).distance > (*b).distance;
    }
};

void Dijkstra(Cluster *src) {
    std::priority_queue<Cluster *, std::vector<Cluster *>, MyCompare> pq;

    src->distance = 0;
    pq.push(src);

    while (!pq.empty()) {
        Cluster *cluster = pq.top();
        pq.pop();

        for (auto i = cluster->neighbors.begin(); i != cluster->neighbors.end(); ++i) {
            Cluster *neighbor = *i;

            int newDistance = cluster->distance;

            if (newDistance < neighbor->distance) {
                neighbor->distance = newDistance;
                pq.push(neighbor);
            }
        }

        for (auto i = cluster->invertedNeighbors.begin(); i != cluster->invertedNeighbors.end(); ++i) {
            Cluster *neighbor = *i;

            int newDistance = cluster->distance + 1;

            if (newDistance < neighbor->distance) {
                neighbor->distance = newDistance;
                pq.push(neighbor);
            }

        }
    }
}