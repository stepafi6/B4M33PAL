#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

int nodeCount, edgeCount, packNodesCount, packEdgesCount;

struct Node {
    int index;
    std::vector<Node *> neighbors;
    std::vector<bool> label;
    bool visited;
};

std::vector<Node> graph;

std::vector<Node *> GetLeafNodes(Node *node);

int main() {
    std::scanf("%d %d %d %d", &nodeCount, &edgeCount, &packNodesCount, &packEdgesCount);

    graph = std::vector<Node>(nodeCount);
    for (int factoryIndex = 0; factoryIndex < nodeCount; ++factoryIndex) {
        graph[factoryIndex].index = factoryIndex;
        graph[factoryIndex].label.push_back(false);
        graph[factoryIndex].label.push_back(true);
    }

    int source, dest;
    for (int i = 0; i < edgeCount; ++i) {
        std::scanf("%d %d", &source, &dest);

        graph[source].neighbors.push_back(&graph[dest]);
        graph[dest].neighbors.push_back(&graph[source]);
    }

    int counter = nodeCount;
    while (counter > 2) {
        std::set<Node *> rings;
        for (auto &node: graph) {
            if (node.neighbors.size() == 1) {
                rings.insert(node.neighbors[0]);
                node.neighbors.clear();
            }
        }

        std::map<Node *, std::vector<Node *>> merges;
        for (auto node: rings)
            merges.insert({node, GetLeafNodes(node)});

        for (auto merge: merges) {
            auto node = merge.first;
            auto leafs = merge.second;

            std::vector<std::vector<bool>> newCert;
            //Check if certificate of the current parent has at least two characters, if so, take it without surrounding 01
            if (node->label.size() > 2)
                newCert.emplace_back(node->label.begin() + 1, node->label.end() - 1);

            //Take the children labels
            for (auto leaf: leafs) {
                newCert.push_back(leaf->label);
            }

            //Sort them lexicographically
            std::sort(newCert.begin(), newCert.end());

            //Build the new label
            node->label.clear();
            node->label.push_back(false);
            for (auto certPart: newCert) {
                node->label.insert(node->label.end(), certPart.begin(), certPart.end());
            }
            node->label.push_back(true);

            counter -= leafs.size();

            for (auto leaf: leafs) {
                auto it = std::find(node->neighbors.begin(), node->neighbors.end(), leaf);
                node->neighbors.erase(it);
            }
        }

        if (counter <= 2) {
            std::vector<std::vector<bool>> result;
            for (auto &node: rings) {
                result.push_back(node->label);
            }

            std::sort(result.begin(), result.end());

            for (auto arr: result)
                for (auto bit: arr)
                    std::cout << bit;

            std::cout << std::endl;
        }
    }


    return 0;
}

std::vector<Node *> GetLeafNodes(Node *node) {
    std::vector<Node *> leafs;

    for (auto neighbor: node->neighbors) {
        if (neighbor->neighbors.size() < 2) {
            leafs.push_back(neighbor);
        }
    }

    return leafs;
}


