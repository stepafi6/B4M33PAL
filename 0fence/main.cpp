#include <iostream>
#include <vector>
#include <complex>

struct point {
    int x;
    int y;
} ;

double CalculatePointDistance(point a, point b){
    return std::sqrt(std::pow(b.x-a.x,2)+std::pow(b.y-a.y,2));
}

const int scale = 5000;

int main() {
    int count;

    scanf("%i", &count);

    std::vector<point> points;
    int x,y;
    for (int i = 0; i < count; i++) {
        scanf("%i %i", &x,&y);
        points.push_back({x,y});
    }

    double perimeter = 0;
    for (int i = 0; i < count - 1; ++i) {
        perimeter += CalculatePointDistance(points[i],points[i+1]);
    }
    perimeter += CalculatePointDistance(points[count - 1], points[0]);

    double fenceLengthPrecise = (perimeter * scale) / 1000;
    int fenceLength = (int)std::ceil(fenceLengthPrecise);

    printf("%d", fenceLength);

    return 0;
}


