#include <iostream>
#include <map>
#include <vector>
#include <valarray>
#include <langinfo.h>
#include <set>

struct State {
    int index;
    std::vector<State *> map;

};

int stateCount, alphabetSize, finalsCount, positiveCount, negativeCount, walkLength;
std::vector<State> states;
std::vector<std::vector<char>> positives;
std::vector<std::vector<char>> negatives;

int main() {
    std::scanf("%d %d %d %d %d %d", &stateCount, &alphabetSize, &finalsCount, &positiveCount, &negativeCount,
               &walkLength);

    states = std::vector<State>(stateCount);

    for (int i = 0; i < stateCount; ++i) {
        int index;
        std::cin >> index;
        states[i].index = index;
        states[i].map = std::vector<State*>(alphabetSize);

        for (int j = 0; j < alphabetSize; ++j) {
            std::cin >> index;

            states[i].map[j] = &states[index];
        }
    }

    std::string line;
    positives = std::vector<std::vector<char>>(positiveCount);
    for (int i = 0; i < positiveCount; ++i) {
        std::cin >> line;

        positives[i] = std::vector<char>(line.begin(), line.end());
    }

    negatives = std::vector<std::vector<char>>(negativeCount);
    for (int i = 0; i < negativeCount; ++i) {
        std::cin >> line;
        negatives[i] = std::vector<char>(line.begin(), line.end());
    }

    for (int startIndex = 0; startIndex < stateCount; ++startIndex) {
        std::set<int> destinations;
        for (int positiveIndex = 0; positiveIndex < positiveCount; ++positiveIndex) {
            State* destination =  &states[startIndex];
            for (auto path : positives[positiveIndex]) {
                int step = path - 'a';
                destination = destination->map[step];
            }
            destinations.insert(destination->index);
        }

        if (destinations.size() != finalsCount)
            continue;

        std::cout << startIndex;
        for (auto dest : destinations) {
            std::cout << " " << dest;
        }
        std::cout << std::endl;
    }

    return 0;
}