cmake_minimum_required(VERSION 3.0)
project(4IncomplAt)

set(CMAKE_CXX_STANDARD 11)

add_executable(4IncomplAt main.cpp)
