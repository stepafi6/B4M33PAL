#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <algorithm>
#include <set>
#include <chrono>

int statesCount;
int alphabetSize;

struct State {
    int index;
    bool isFinite;
    int max_depth = INT32_MIN;
    int min_depth = INT32_MAX;
    std::map<char, std::set<State *>> transitions;
    std::set<State *> reachableStates;
};

void find_max_depth(State *state, int depth);
void find_min_depth(State *state, int depth);

void walk_prefix(State *state, std::vector<char> prefix, std::set<State *> &resultStates);

void log_max_depth(int depth);
void log_min_depth(int depth);

std::vector<State> states;
int max_depth = INT32_MIN;
int min_depth = INT32_MAX;

bool is_number(const std::string &s) {
    return !s.empty() && std::all_of(s.begin(), s.end(), ::isdigit);
}

int main() {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::scanf("%d %d", &statesCount, &alphabetSize);

    states = std::vector<State>(statesCount);

    std::string xx;
    getline(std::cin, xx);

    for (int nodeIndex = 0; nodeIndex < statesCount; ++nodeIndex) {
        std::string line;
        getline(std::cin, line);
        std::stringstream iss(line);

        states[nodeIndex].index = nodeIndex;

        std::string node;
        iss >> node;

        std::string finiteChar;
        iss >> finiteChar;
        states[nodeIndex].isFinite = finiteChar == "F";

        char label;
        std::string token;

        while (iss >> token) {
            if (is_number(token)) {
                int destinationIndex = std::stoi(token);
                states[nodeIndex].transitions[label].insert(&states[destinationIndex]);
                states[nodeIndex].reachableStates.insert(&states[destinationIndex]);
            } else {
                label = token[0];
            }
        };
    }

    std::string prefixLine;
    getline(std::cin, prefixLine);
    std::vector<char> prefix(prefixLine.begin(), prefixLine.end());

    //Find paths
    std::set<State *> postPrefixStates;
    walk_prefix(&states[0], prefix, postPrefixStates);

    int initDepth = prefix.size();
    for (auto post: postPrefixStates) {
        find_max_depth(post, initDepth);
    }

    for (auto post: postPrefixStates) {
        find_min_depth(post, initDepth);
    }

    std::cout << min_depth << " " << max_depth << std::endl;

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Total = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;
    return 0;
}

void walk_prefix(State *state, std::vector<char> prefix, std::set<State *> &resultStates) {
    if (prefix.empty()) {
        resultStates.insert(state);
        return;
    }

    for (auto x: state->transitions[prefix[0]]) {
        walk_prefix(x, std::vector<char>(prefix.begin() + 1, prefix.end()), resultStates);
    }
}

void find_max_depth(State *state, int depth) {
    state->max_depth = depth;

    if (state->isFinite) {
        log_max_depth(depth);
    }

    for (auto x: state->reachableStates) {
        if (x->max_depth >= depth + 1)
            continue;
        else
            find_max_depth(x, depth + 1);
    }
}

void find_min_depth(State *state, int depth) {
    state->min_depth = depth;

    if (state->isFinite) {
        log_min_depth(depth);
        return;
    }

    for (auto x: state->reachableStates) {
        if (x->min_depth <= depth + 1)
            continue;
        else
            find_min_depth(x, depth + 1);
    }
}

void log_max_depth(int depth) {
    if (depth > max_depth)
        max_depth = depth;
}

void log_min_depth(int depth) {
    if (depth < min_depth)
        min_depth = depth;
}

