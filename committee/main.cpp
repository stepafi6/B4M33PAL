#include <iostream>
#include <set>
#include <vector>
#include <math.h>
#include <deque>
#include <chrono>

using namespace std;

std::string alphabet;
int membersCount;
int mustAgree;
int wordLength;


// trie node
struct TrieNode {
    explicit TrieNode(int alphabetSize): children(alphabetSize) {
        for (int i = 0; i < alphabetSize; i++)
            children[i] = nullptr;

        isEndOfWord = false;
    }

    std::vector<TrieNode *> children;

    // isEndOfWord is true if the node represents
    // end of a word
    bool isEndOfWord;

    std::set<int> members;
};


struct Trie {
    const int alphabetSize;
    TrieNode* root;

    Trie(int _alphabetSize) : alphabetSize(_alphabetSize){
        root = new TrieNode(_alphabetSize);
    }

    // If not present, inserts key into trie
    // If the key is prefix of trie node, just
    // marks leaf node
    void insert(string key, int member) {
        struct TrieNode *pCrawl = root;

        for (int i = 0; i < key.length(); i++) {
            int index = key[i] - 'a';
            if (!pCrawl->children[index])
                pCrawl->children[index] = new TrieNode(alphabetSize);

            pCrawl = pCrawl->children[index];
        }

        // mark last node as leaf
        pCrawl->isEndOfWord = true;
        pCrawl->members.insert(member);
    }

    void find_patterns(TrieNode* node, std::vector<std::string> &matches, std::string pattern) {
        if (node->members.size() >= mustAgree) {
            matches.push_back(pattern);
            return;
        }

        for (int i = 0; i < alphabetSize; i++) {
            if (node->children[i]) {
                char c = i + 97;
                std::string ss = pattern + c;
                find_patterns(node->children[i], matches, ss);
            }
        }
    }
};


int powModulo(int a, int b, int n) {
    long long x = 1, y = a;
    while (b > 0) {
        if (b % 2 == 1) {
            x = (x * y) % n; // multiplying with base
        }
        y = (y * y) % n; // squaring the base
        b /= 2;
    }
    return x % n;
}

// Driver
int main() {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    char tmp[1000000];
    std::scanf("%s %d %d %d", tmp, &membersCount, &mustAgree, &wordLength);
    alphabet = tmp;
    int alphabetSize = alphabet.length();

    std::vector<std::set<std::string>> membersPatterns;

    for (int memberIndex = 0; memberIndex < membersCount; ++memberIndex) {
        int patternsCount;
        std::scanf("%d", &patternsCount);

        std::set<std::string> patterns;
        for (int patternIndex = 0; patternIndex < patternsCount; ++patternIndex) {
            std::scanf("%s", tmp);
            patterns.insert(tmp);
        }
        membersPatterns.push_back(patterns);
    }

    Trie* trie = new Trie(26);

    for (int memberIndex = 0; memberIndex < membersCount; ++memberIndex) {
        for (auto pattern: membersPatterns[memberIndex]) {
            trie->insert(pattern, memberIndex);
        }
    }

    std::vector<std::string> foos;
    trie->find_patterns(trie->root, foos, "");

    long sum = 0;
    for (auto foo: foos) {
        int diff = wordLength - foo.length();
        sum += powModulo(alphabet.length(), diff, 100000);
        sum = sum % 100000;
    }

    std::cout << sum % 100000 << std::endl;
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;
    return 0;
}