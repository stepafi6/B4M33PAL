cmake_minimum_required(VERSION 3.16)
project(committee)

set(CMAKE_CXX_STANDARD 14)

add_executable(committee main.cpp)
