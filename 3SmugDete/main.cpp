#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

struct Pack;

int nodeCount, edgeCount, packNodesCount, packEdgesCount;
int packId = 0;
std::vector<std::vector<bool>> adjMatrix;
std::vector<int> nodes;
std::vector<Pack> packs;

struct Pack {
    int id;
    std::vector<int> nodes;
    std::vector<bool> inPack;

    Pack(int id, std::vector<int> nodes) : id(id), nodes(std::move(nodes)) {

    }
};

void GeneratePacks(int rank, int dataIndex, std::vector<int> data, int nodeIndex);

bool ValidatePack(Pack pack);

int CalculateDegree(int node, Pack pack);

bool IsIsomorphic(Pack pack, Pack otherPack);

std::vector<bool> BFS(int start, Pack pack);

int main() {
    std::scanf("%d %d %d %d", &nodeCount, &edgeCount, &packNodesCount, &packEdgesCount);

    adjMatrix = std::vector<std::vector<bool>>(nodeCount);
    nodes = std::vector<int>(nodeCount);
    for (int i = 0; i < nodeCount; ++i) {
        adjMatrix[i] = std::vector<bool>(nodeCount);
        nodes[i] = i;
    }

    int source, dest;
    for (int i = 0; i < edgeCount; ++i) {
        std::scanf("%d %d", &source, &dest);

        adjMatrix[source][dest] = true;
        adjMatrix[dest][source] = true;
    }

    auto data = std::vector<int>(packNodesCount);
    GeneratePacks(packNodesCount, 0, data, 0);

    for (const auto &pack: packs) {
        for (const auto &otherPack: packs) {
            if (IsIsomorphic(pack, otherPack)) {
                for (int i = 0; i < pack.nodes.size(); ++i) {
                    if (i != 0)
                        std::cout << " ";

                    std::cout << pack.nodes[i];
                }
                std::cout << " ";
                for (int i = 0; i < otherPack.nodes.size(); ++i) {
                    if (i != 0)
                        std::cout << " ";

                    std::cout << otherPack.nodes[i];
                }
                std::cout << std::endl;
            }
        }
    }

    return 0;
}

bool IsIsomorphic(Pack pack, Pack otherPack) {
    if (pack.id >= otherPack.id)
        return false;

    std::vector<int> v3;
    std::set_intersection(pack.nodes.begin(), pack.nodes.end(),
                          otherPack.nodes.begin(), otherPack.nodes.end(),
                          back_inserter(v3));
    if (!v3.empty())
        return false;

    auto degrees = std::vector<int>(nodeCount);

    for (int i = 0; i < pack.nodes.size(); ++i) {
        int node = pack.nodes[i];
        int otherNode = otherPack.nodes[i];

        degrees[node] = CalculateDegree(node, pack);
        degrees[otherNode] = CalculateDegree(otherNode, otherPack);
    }

    auto adjDegrees = std::vector<std::pair<int, std::vector<int>>>();
    for (int node: pack.nodes) {
        auto costs = std::vector<int>();
        for (int neighbor = 0; neighbor < nodeCount; ++neighbor) {
            if (!adjMatrix[node][neighbor] || !pack.inPack[neighbor])
                continue;
            costs.push_back(degrees[neighbor]);
        }
        adjDegrees.emplace_back(degrees[node], costs);
    }

    auto otherAdjDegrees = std::vector<std::pair<int, std::vector<int>>>();
    for (int node: otherPack.nodes) {
        auto costs = std::vector<int>();
        for (int neighbor = 0; neighbor < nodeCount; ++neighbor) {
            if (!adjMatrix[node][neighbor] || !otherPack.inPack[neighbor])
                continue;
            costs.push_back(degrees[neighbor]);
        }
        otherAdjDegrees.emplace_back(degrees[node], costs);
    }

    std::sort(adjDegrees.begin(), adjDegrees.end());
    std::sort(otherAdjDegrees.begin(), otherAdjDegrees.end());


    for (auto & adjDegree : adjDegrees) {
        auto it = otherAdjDegrees.begin();
        while (it != otherAdjDegrees.end()) {
            auto otherAdj = *it;
            if (adjDegree.first != otherAdj.first )
                return false;

            auto degreeList = adjDegree.second;
            auto otherDegreeList = otherAdj.second;

            std::sort(degreeList.begin(), degreeList.end());
            std::sort(otherDegreeList.begin(), otherDegreeList.end());

            if (degreeList != otherDegreeList) {
                it++;
                continue;
            } else {
                otherAdjDegrees.erase(it);
                break;
            }
        }
    }
    return true;
}

int CalculateDegree(int node, Pack pack) {
    int degree = 0;
    for (int i = 0; i < nodeCount; ++i) {
        if (node == i)
            continue;
        else if (adjMatrix[node][i] && pack.inPack[i])
            degree++;

    }
    return degree;
}

void GeneratePacks(int rank, int dataIndex, std::vector<int> data, int nodeIndex) {
    if (dataIndex == rank) {
        Pack pack = Pack(packId, data);

        pack.inPack = std::vector<bool>(nodeCount);
        for (auto node: pack.nodes) {
            pack.inPack[node] = true;
        }

        if (ValidatePack(pack)) {
            packs.push_back(pack);
            packId++;
        }
        return;
    }

    if (nodeIndex >= nodeCount)
        return;

    data[dataIndex] = nodes[nodeIndex];
    GeneratePacks(rank, dataIndex + 1, data, nodeIndex + 1);
    GeneratePacks(rank, dataIndex, data, nodeIndex + 1);
}

bool ValidatePack(Pack pack) {
    int pathsCount = 0;

    for (auto node: pack.nodes) {
        for (auto neighbor: pack.nodes) {
            if (node == neighbor)
                continue;

            if (adjMatrix[node][neighbor]) {
                pathsCount++;
            }
        }
    }

    auto visited = BFS(pack.nodes[0], pack);

    for (int i = 0; i < nodeCount; ++i) {
        if (pack.inPack[i] && !visited[i])
            return false;
    }

    return pathsCount / 2 == packEdgesCount;
}

std::vector<bool> BFS(int start, Pack pack) {
    std::vector<bool> visited(nodeCount, false);
    std::vector<int> q;
    q.push_back(start);

    visited[start] = true;

    int vis;
    while (!q.empty()) {
        vis = q[0];

        q.erase(q.begin());

        for (int i = 0; i < nodeCount; i++) {
            if (adjMatrix[vis][i] && (!visited[i]) && pack.inPack[i]) {
                q.push_back(i);
                visited[i] = true;
            }
        }
    }

    return visited;
}