import networkx as nx
import matplotlib.pyplot as plt

file1 = open('test.in', 'r')
 
headerline = file1.readline()
print(headerline)

header = headerline.split()
factoryCount = int(header[0])
convCount = int(header[1])
mainFactory = int(header[2])

G = nx.Graph()

for i in range(factoryCount):
    G.add_node(i)

edges = []
arrw = [[] for i in range(factoryCount)]
for i in range(convCount):
    edgeText = headerline = file1.readline()
    edge = edgeText.split()
    arrw[int(edge[0])].append(int(edge[1]))
    G.add_edge(int(edge[0]),int(edge[1]))

G.add_edges_from(edges)


pos = nx.spring_layout(G, iterations=20, seed=39775)
nx.draw(G, pos=pos, with_labels=True, font_weight='bold', node_size=factoryCount)

labels = nx.get_edge_attributes(G,'weight')
nx.draw_networkx_edge_labels(G,pos,edge_labels=labels)


plt.show()

