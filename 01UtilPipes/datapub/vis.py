import networkx as nx
import matplotlib.pyplot as plt

file1 = open('pub04.in', 'r')
 
headerline = file1.readline()
print(headerline)

header = headerline.split()
farmsCount = int(header[0])
roadsCount = int(header[1])

G = nx.Graph()

for i in range(farmsCount):
    G.add_node(i)

edges = []
for i in range(roadsCount):
    edgeText = headerline = file1.readline()
    edge = edgeText.split()
    G.add_edge(int(edge[0]),int(edge[1]), weight=int(edge[2]))

hubFarmsCount = int(file1.readline())
hubFarms = []
hubFarmsLine = file1.readline()
for hubFarm in hubFarmsLine.split():
    hubFarms.append(int(hubFarm))

G.add_edges_from(edges)


pos = nx.spring_layout(G, iterations=100, seed=39775)
nx.draw(G, pos=pos, with_labels=True, font_weight='bold', node_size=farmsCount)

labels = nx.get_edge_attributes(G,'weight')
nx.draw_networkx_edge_labels(G,pos,edge_labels=labels)

for hubFarm in hubFarms:
    nx.draw(G.subgraph(hubFarm), pos=pos, font_size=16, node_color='red', font_color='green')

plt.show()  