using namespace std;

#include <iostream>
#include <list>
#include <algorithm>
#include <array>
#include <vector>
#include <queue>
#include <chrono>
#include <map>
#include <functional>


# define INF 0x3f3f3f3f

typedef std::pair<int, int> iNode;


struct Node {
    int index;
    bool isHub;
    int rootHub = -1;
    bool isUndecided = false;
    bool inMST = false;
    int distance = INF;
    std::map<Node *, int> neighbors;

    bool operator<(const Node &rhs) const {
        return distance < rhs.distance;
    }

    bool operator>(const Node &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Node &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Node &rhs) const {
        return !(*this < rhs);
    }

};

struct Road {
    int src;
    int dest;
    int weight;

    Road(int src, int dest, int weight) : src(src), dest(dest), weight(weight) {}
};

class MyCompare {
public:
    template<typename T>
    bool operator()(T *a, T *b) {
        return (*a) > (*b);
    }
};

int farmsCount;
int roadCount;
int hubFarmsCount;
std::vector<int> hubFarms;
std::vector<Road> roads;

std::vector<Node> graph;

int Prim(int src);

void Dijkstra();

int main() {
    //auto start = std::chrono::high_resolution_clock::now();
    scanf("%i %i", &farmsCount, &roadCount);

    graph = std::vector<Node>(farmsCount);
    roads = std::vector<Road>();

    for (int i = 0; i < farmsCount; ++i)
        graph[i].index = i;


    for (int i = 0; i < roadCount; i++) {

        int u;
        int v;
        int w;

        scanf("%i %i %i", &u, &v, &w);
        roads.emplace_back(u, v, w);
    }

    scanf("%i", &hubFarmsCount);

    char temp;
    int hubFarm;
    for (int i = 0; i < hubFarmsCount; i++) {
        scanf("%d%c", &hubFarm, &temp);
        hubFarms.emplace_back(hubFarm);
    }

    for (const Road road: roads) {
        graph[road.src].neighbors.emplace(&graph[road.dest], road.weight);
        graph[road.dest].neighbors.emplace(&graph[road.src], road.weight);
    }

    for (int hubFarm: hubFarms) {
        graph[hubFarm].isHub = true;
        graph[hubFarm].distance = 1;
        graph[hubFarm].rootHub = hubFarm;
    }

    Dijkstra();

    vector <Node> undecidedFarms;
    for (int u = 0; u < farmsCount; u++) {
        if (graph[u].isUndecided) {
            undecidedFarms.emplace_back(graph[u]);
            graph[u].neighbors.clear();
            continue;
        }

        std::map<Node *, int>::iterator v = graph[u].neighbors.begin();
        while (v != graph[u].neighbors.end()) {
            if (v->first->isUndecided || v->first->rootHub != graph[u].rootHub)
                v = graph[u].neighbors.erase(v);
            else
                v++;
        }
    }

    /*
    for (Node node: graph) {
        for (auto neighbor: node.neighbors) {
            cout << node.index << " " << neighbor.first->index << " " << neighbor.second << endl;
        }
    }*/

    int sum = 0;
    for (int hubFarmIndex = 0; hubFarmIndex < hubFarmsCount; hubFarmIndex++) {
        sum += Prim(hubFarms[hubFarmIndex]);
    }

    /*
    for (Node tu: graph) {
        sum += tu.tube;
        //printf("%d-%d for %d \n",tu.index, tu.parent, tu.tube);
    }*/

    cout << sum << " " << undecidedFarms.size() << endl;
    //auto end = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    //cout << duration.count()/1000 << " ms" << endl;

    return 0;
}

void Dijkstra() {
    const int src = -1;

    Node fake = Node();
    fake.index = -1;
    fake.rootHub = -1;
    fake.distance = 0;

    for (int i = 0; i < hubFarmsCount; ++i) {
        fake.neighbors.emplace(&graph[hubFarms[i]], 0);
    }

    std::priority_queue<Node *, vector < Node * >, MyCompare >
                                                   pq;
    pq.push(&fake);
    while (!pq.empty()) {
        Node *node = pq.top();
        pq.pop();

        std::map<Node *, int>::iterator i;
        for (i = node->neighbors.begin(); i != node->neighbors.end(); ++i) {
            Node *neighbor = i->first;
            int weight = i->second;

            int newDistance = node->distance + weight;

            if (newDistance < neighbor->distance) {
                neighbor->distance = newDistance;

                //Svagr fix
                neighbor->isUndecided = node->isUndecided;

                if (node->rootHub != src)
                    neighbor->rootHub = node->rootHub;

                pq.push(neighbor);
            } else if (newDistance == neighbor->distance) {
                if (neighbor->rootHub != node->rootHub)
                    neighbor->isUndecided = true;

                neighbor->rootHub = node->rootHub;

                pq.push(neighbor);
            }

        }
    }
}

typedef pair<long long, Node *> PII;

int Prim(int src) {
    priority_queue <PII, vector<PII>, greater<PII>> Q;
    int y;
    int minimumCost = 0;
    PII p;
    Q.push(make_pair(0, &graph[src]));
    while (!Q.empty()) {
        p = Q.top();
        Q.pop();
        Node *x = p.second;
        if (x->inMST == true)
            continue;
        minimumCost += p.first;
        x->inMST = true;

        std::map<Node *, int>::iterator i;
        for (i = x->neighbors.begin(); i != x->neighbors.end(); ++i) {
            {
                Node *y = i->first;
                if (y->isUndecided == false)
                    Q.push(make_pair(i->second, y));
            }
        }
    }

    return minimumCost;
}
